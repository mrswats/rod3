import json
import os

import dotenv
import requests


dotenv.load_dotenv()


TWITTER_API_BEARER_TOKEN = os.getenv("TWITTER_API_BEARER_TOKEN")


def main():

    headers = {"Authorization": f"Bearer {TWITTER_API_BEARER_TOKEN}"}
    params = {"query": "#rod3"}
    # No funciona per que no tinc aacces a un projecte academic de recerca
    # twitter_search_endpoint = "https://api.twitter.com/2/tweets/search/all"
    # Aquest nomes busca als ultims set dies
    twitter_search_endpoint = "https://api.twitter.com/2/tweets/search/recent"
    response = requests.get(twitter_search_endpoint, params=params, headers=headers)

    print(json.dumps(response.json()))


if __name__ == "__main__":
    main()
