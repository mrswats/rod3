# Search Twitter

Fa una cerca a twitter amb una query, en aquest cas `#rod3` dels ultims set dies.

El projecte hauria de funcionar amb python 3.7+

## Autenticacico

Primer, necessitaras un fitxer anomenat `.env` al mateix directori que contingui el token BEARER
que et proporciona Twitter a https://developer.twitter.com/en/portal/dashboard despres de crear un projecte
i agafant les claus corresponents.

## Dependencies

Has d'instalar les dependencies dins un entorn virtual:
```bash
python -m venv venv
source ./venv/bin/activate
pip install -r requirements.txt
````

## Execucio del Script

un cop fet aixo pots executar el programa amb:
```bash
python twitter.py
```

que imprimira un JSON per pantalla
